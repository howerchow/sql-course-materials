-- 查看表结构
SHOW CREATE TABLE sql_hr.employees;

-- 创建/删除表
CREATE TABLE IF NOT EXISTS employees_back(
employee_id INT NOT NULL,
first_name VARCHAR(50) NOT NULL,
last_name VARCHAR(50),
reports_to INT,
office_id INT
)

DROP TABLE employees_back;

-- 创建表(根据现有的表)
CREATE TABLE employees_back
AS
SELECT employee_id, first_name, last_name, reports_to, office_id
FROM employees
WHERE office_id <= 3;

DESC employees_back
SELECT * FROM employees_back;
-- 修改表 增/删字段
ALTER TABLE employees_back
ADD salary DOUBLE(10,2) AFTER last_name;

ALTER TABLE employees_back
DROP salary;

-- 修改表 修改数据 类型 默认值
ALTER TABLE employees_back
MODIFY last_name VARCHAR(35) DEFAULT "AAA";

-- 修改表 重名字段
ALTER TABLE employees_back
CHANGE salary month_salary DOUBLE(10,2);

-- 重命名表
RENAME TABLE employees_back
TO employees_my;

-- 清空表 保留表结构
TRUNCATE TABLE employees_back;

-- 添加数据
INSERT INTO employees_back
VALUES (77777, "TOM", "HANKS", 37270, 1);

INSERT INTO employees_back (first_name, last_name, employee_id, reports_to, office_id)
VALUES ("JERRY", "LEE", 54231, 37270, 3);

INSERT INTO employees_back
VALUES (88881, "ROSE", "HANKS", 37270, 1),
       (88882, "JACK", "WONG", 37270, 3);

INSERT INTO employees_back(employee_id, first_name,last_name,reports_to,office_id)
SELECT employee_id, first_name,last_name,reports_to,office_id
FROM employees
WHERE office_id <= 3;

SELECT * FROM employees_back;
DESC employees_back;

-- 更新数据
UPDATE employees_back
SET reports_to = 27370, office_id =  2
WHERE employee_id = 76196

-- 删除数据
DELETE FROM offices
WHERE office_id = 10;


-- 约束
-- 单列约束 多列约束
-- 列级约束 表级约束
-- 非空约束 唯一约束 主键约束 检查约束默认值约束

-- CREATE TABLE 添加约束
-- ALTER TABLE  增加约束 删除约束

ALTER TABLE employees_back
MODIFY office_id INT NOT NULL

ALTER TABLE employees
ADD UNIQUE(salary);

show variables like '%innodb_page_size%';


-- show tables;
-- select * from employees;
-- select * from offices;
SHOW INDEX IN employees;
